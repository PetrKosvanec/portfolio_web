var customName = document.getElementById('customname');
var randomize = document.querySelector('.randomize');
var story = document.querySelector('.story');

var storyText = 'It was 94 fahrenheit outside, so :insertx: went for a walk. When they got to :inserty:, they stared in horror for a few moments, then :insertz:. Bob saw the whole thing, but was not surprised — :insertx: weighs 300 pounds, and it was a hot day.';
var insertX = ['Willy the Goblin', 'Big Daddy', 'Father Christmas'];
var insertY = ['the soup kitchen', 'Disneyland', 'the White House'];
var insertZ = ['spontaneously combusted', 'melted into a puddle on the sidewalk', 'turned into a slug and crawled away'];

function randomValueFromArray(array){
  return array[Math.floor(Math.random()*array.length)];
}

randomize.addEventListener('click', result);

function result() {
  var newStory = storyText;
  var xItem = randomValueFromArray(insertX);
  var yItem = randomValueFromArray(insertY);
  var zItem = randomValueFromArray(insertZ);

  if(customName.value !== '') {
    var name = customName.value;
    var newStory = newStory.replace(/Bob/g, name) + ''; // var newStBob = newStory.replace(/Bob/g, name);
  }

  if(document.getElementById("uk").checked) {
    var weight = Math.round(300/14) + ' stone';
    var temp = Math.round((94 - 32) * 5/9) + ' celsius';

    newStory = newStory.replace(/94 fahrenheit/g, temp).replace(/300 pounds/g, weight) + '';
  }

  newStory = newStory.replace(/:insertx:/g,xItem).replace(/:inserty:/g,yItem).replace(/:insertz:/g,zItem) + '';
  
  story.textContent = newStory;
  story.style.visibility = 'visible';
}
