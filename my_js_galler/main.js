var displayedImage = document.querySelector('.displayed-img');
var thumbBar = document.querySelector('.thumb-bar');
var imgToFire;

var btn = document.querySelector('button');
var overlay = document.querySelector('.overlay');

/* Looping through images */
for (let i=0; i < 5; i++) {
	var newImage = document.createElement('img');
	newImage.setAttribute('src', './images/pic' + (i+1) + '.jpg');
	thumbBar.appendChild(newImage);
}

/* Wiring up the Darken/Lighten button */
btn.onclick = function(e) {
	if (
		e.target.getAttribute("class") === "dark") {
		btn.setAttribute('class', "light");
		btn.textContent = "Lighten";
		overlay.style.backgroundColor = "rgba(0,0,0,0.5)";
	}
	else {
		btn.setAttribute('class', "dark");
		btn.textContent = "Darken";
		overlay.style.backgroundColor = "rgba(0,0,0,0)";
	}
}

/* Opening the thumb-pic into the Main Image */
thumbBar.addEventListener("click", function whichImgToFire(e) {
	if (e.target && e.target.nodeName == "IMG") {
		displayedImage.src = "./" + e.target.src.slice(-15);
	}
});
